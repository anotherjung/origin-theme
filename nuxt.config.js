import consola from 'consola'
import get from 'lodash/get'
import { toCamel } from 'swell-js/dist/utils'
//dotnet import settings from './config/settings.json'
const storeMap = require('./config/store-map.js')
const settings = storeMap[process.env.SWELL_NAME]
import menuSettings from './config/menus.json'
import { getGoogleFontConfig } from './modules/swell-editor/utils'
import { getLangSettings } from './modules/swell-editor/lang/utils'
import { mergeSettings } from './modules/swell/utils/mergeSettings'

const logger = consola.withScope('swell-editor')

const isProduction = process.env.NODE_ENV === 'production'
const editorMode = process.env.SWELL_EDITOR === 'true'

if (editorMode) {
  logger.info('Swell Editor enabled')
}


export default async () => {
  const mergedSettings = await mergeSettings(toCamel(settings))
  const mergedMenuSettings = await mergeSettings(toCamel(menuSettings))
  const storeId = get('mergedSettings', 'store.id')

  return {
    head: { link: [{ rel: 'icon', type: 'image/png', href: '/favicon.png' }] },
    
    vue: {
      config: {
        devtools: !isProduction,
        productionTip: false,
      },
    },

    target: 'static',

    /*
     ** Make all components in the /components folder available in templates without
     *  needing to import them explicitly or define them on the Vue instance object.
     */
    components: true,

    /*
     ** Set the progress-bar color
     */
    loading: { color: get(mergedSettings, 'colors.accent'), continuous: true },

    /*
     ** Vue plugins to load before mounting the App
     */
    plugins: [
      { src: '~/plugins/vue-country-region-select', mode: 'client' },
      { src: '~/plugins/vue-credit-card-validation', mode: 'client' },
      { src: '~/plugins/directives', mode: 'client' },
      { src: '~/plugins/swell-lang.js' },
      { src:   '~/plugins/gtm'}
    ],

    /*
     ** Nuxt.js modules
     */
    modules: [
      ['@nuxtjs/gtm'],
      ['@nuxtjs/sentry'],
      ['@nuxtjs/sitemap'],
    ],

    buildModules: [
      [
        '@nuxtjs/pwa', 
        {
          manifest: {
            name: get(mergedSettings, 'store.name'),
            short_name: get(mergedSettings, 'store.name'),
            description: "Fishermen Fresh",
            lang: "en",
            dir: "ltr",
            scope: "/",
            start_url: "/?utm_source=pwa",
            display: "fullscreen",
            orientation: "portrait",
            theme_color: "black",
            background_color: "white",
            useWebmanifestExtension: false,
            icons: []
          },
          meta: {
            name: get(mergedSettings, 'store.name'),
            title: get(mergedSettings, 'store.name'),
            description: get(mergedSettings, 'store.description'),
            author: "Ahi Assassins",
            theme_color: "black",
            lang: "en",
            ogType: "website",
            ogSiteName: get(mergedSettings, 'store.name'),
            ogTitle: get(mergedSettings, 'store.name'),
            ogDescription: get(mergedSettings, 'store.description'),
            ogHost: get(mergedSettings, 'store.domain'),
            appleStatusBarStyle: "black",
            favicon: ['favicon.ico','apple-touch-icon.png'],
            nativeUI: false,
            mobileAppIOS: 'apple-mobile-web-app-capable',
            ogImage: '~/static/ahi.png', 
            options: ['~/static/ahi.png', [720,720], 'image/png']
          },
          workbox: {
            runtimeCaching: [
              {
                urlPattern: 'https://cdn.schema.io/*',
              },
            ],
          },
          icons: {
            source: '~/static/ahi.png',
            fileName: "ahi.png",
            sizes: [64, 120, 144, 152, 180,167, 192, 384, 512],
            pluginName:'$icon',
            purpose: 'maskable',
            cacheDir: '~/node_modules/.cache/pwa/icon',
            targetDir: 'icons',
            plugin: true,
          }
        }
      ],

      ['nuxt-i18n'],

      ['~/modules/swell/utils/generateDynamicRoutes',
      ],

      ['@nuxtjs/tailwindcss',
        {
          // Put your config overrides here
        },
      ],

      [
        '@nuxtjs/google-fonts',
        getGoogleFontConfig(settings),
      ],

      [
        '~/modules/swell-editor',
        /*
         ** Provides communication and utilitiy functions for interfacing
         *  with Swell's storefront editor and fetching settings/content.
         *
         * IMPORTANT: the swell module must come after this one, otherwise everything breaks.
         * If you aren't using the storefront editor, this module can be safely removed.
         */
        {
          useEditorSettings: editorMode,
        },
      ],

      [
        '~/modules/swell',
        /*
         ** Initializes Swell.js SDK and injects it into Nuxt's context.
         *
         *  If you've cloned this repository from your store dashboard,
         *  these settings will already be configured in config/settings.json.
         *
         *  You can optionally override them here or using environment variables.
         *  https://github.com/swellstores/swell-theme-origin#configuration
         */
        {
          storeId: process.env.SWELL_STORE_ID,
          publicKey: process.env.SWELL_PUBLIC_KEY,
          previewContent: editorMode,
          storeUrl: process.env.SWELL_STORE_URL,
          currentSettings: {
            settings: mergedSettings,
            menus: mergedMenuSettings,
          },
        },
      ],

    ],

    gtm: {
      id: process.env.GOOGLE_GTM,
      enabled: true,
      debug: false,
      layer: 'dataLayer',
      variables: {},
      pageTracking: true,
      pageViewEventName: 'nuxtRoute',
      autoInit: true,
      respectDoNotTrack: true,
  
      scriptId: 'gtm-script',
      scriptDefer: false,
      scriptURL: 'https://www.googletagmanager.com/gtm.js',
      crossOrigin: false,
  
      noscript: true,
      noscriptId: 'gtm-noscript',
      noscriptURL: 'https://www.googletagmanager.com/ns.html'
    },

    i18n: await getLangSettings(mergedSettings, editorMode),

    sitemap: {
      hostname: mergedSettings.store.domain,
      gzip: true,
      i18n: true,
      exclude: ['/account/**', '/*/account/**'],
    },
    generate: {
      exclude: [/^\/?([a-z]{2}-?[A-Z]{2}?)?\/account/],
      fallback: true, // Fallback to the generated 404.html
    },

    /*
     ** Extend default Nuxt routes to add page aliases
     */
    router: {
      trailingSlash: true,
      extendRoutes(routes, resolve) {
        // Rewrite to use the pages/_slug.vue component for home page, since the
        // content type is the same. If you want to have a unique template,
        // create a pages/index.vue and remove this route definition.
        routes.push({
          name: 'index',
          path: '/',
          component: resolve(__dirname, 'pages/_slug.vue'),
        })
      },
    },

    /*
     ** Extend default Nuxt server options
     */
    server: {
      host: process.env.HOST || 'localhost',
      port: process.env.PORT || 3333,
    },
  }
}
