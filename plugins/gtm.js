export default function({ $gtm, route }) {
    $gtm.init(process.env.GOOGLE_GTM)
    $gtm.push({ event: 'gtm plugin' })
  }